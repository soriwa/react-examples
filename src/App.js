import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Page from './Page';
import Menu from './Menu';

const App = () => (
  <BrowserRouter>
    <div>
      <Menu/>
      <Page/>
    </div>
  </BrowserRouter>
);

export default App;