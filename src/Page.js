import React from 'react';
import { Route } from 'react-router-dom';
import { FirstApp, JSX, Components, Props, Toggle } from 'pages';

class Page extends React.Component {
  constructor(props) {
    super(props);
    this.state = {  };
  }
  render() {
    return (
      <div>
        <Route path="/first-app" component={FirstApp}/>
        <Route exact path="/" component={JSX}/>
        <Route path="/components" component={Components}/>
        <Route path="/props" component={Props}/>
        <Route path="/toggle" component={Toggle}/>
      </div>
    );
  }
}

export default Page;