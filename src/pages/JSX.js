/**
 * JSX 예제 
 */
import React from 'react';

const user = {
  avatarUrl: 'https://placeimg.com/320/160/nature',
  altText: 'nature'
}

const element1 = <h1>Hello, world!</h1>;
const element2 = <div tabIndex="0" className="tab">attributes</div>;
const element3 = <img src={user.avatarUrl} alt={user.altText}></img>;
const element4 = <img src={user.avatarUrl} alt={user.altText} />;
const element5 = (
  <div>
    <h1>Hello!</h1>
    <h2>Good to see you here.</h2>
  </div>
);

const JSX = () => (
  <div>
    <h1>JSX Examples</h1>
    {element1}
    {element2}
    {element3}
    {element4}
    {element5}
  </div>
);

export default JSX;