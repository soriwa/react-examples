/**
 * 컴포넌트 예제
 * 
 * 함수형 컴포넌트와 클래스 컴포넌트 
 */
import React from 'react';
// import JSX from './JSX';
// import Toggle from './Toggle';

/**
 * 함수형 컴포넌트 
 * @param {*} props 
 */
const Components = (props) => <h1>Hello, {props.name}</h1>

/**
 * 클래스 기반의 컴포넌트
 */
// class Components extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {  };
//   }

//   render() {
//     return (
//       <h1>Hello, {this.props.name}</h1>
//     );
//   }
// }

/**
 * 다른 컴포넌트를 포함한 컴포넌트 
 */
// class Components extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {  };
//   }

//   render() {
//     return (
//       <div>
//         <h1>Hello, {this.props.name}</h1>
//         {/* <Toggle title="toggle button" /> */}
//         <Toggle />
//       </div>
//     );
//   }
// }

Components.defaultProps = {
  name: 'World!'
};

export default Components;