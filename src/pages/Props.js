/**
 * Props 예제
 */
import React, { Component } from 'react';

const Welcome = (props) => (
  <div style={props.style}>
		<h1>Hi {props.name}!</h1>
		{props.children}
  </div>
);

Welcome.defaultProps = {
  name: 'there'
};


class Props extends Component {
  constructor(props) {
    super(props);
    this.state = {  };
  }

  render() {
    return (
      <Welcome style={{background: 'yellow'}} name="guys">
        반가워요!
      </Welcome> 
    );
  }
}



export default Props;