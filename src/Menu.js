import React from 'react';
import { NavLink } from 'react-router-dom';
import './Menu.css';

const activeStyle = {
  backgroundColor: '#111111'
};

const Menu = () => (
  <div>
    <ul>
      <li><NavLink to="/first-app" activeStyle={activeStyle}>First App</NavLink></li>
      <li><NavLink exact to="/" activeStyle={activeStyle}>JSX</NavLink></li>
      <li><NavLink exact to="/components" activeStyle={activeStyle}>Components</NavLink></li>
      <li><NavLink to="/props" activeStyle={activeStyle}>Props</NavLink></li>
      <li><NavLink to="/toggle" activeStyle={activeStyle}>State & Event Handling</NavLink></li>
    </ul>
  </div>
);

export default Menu;